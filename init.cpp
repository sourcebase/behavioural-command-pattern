#include<iostream>
using namespace std ;
//Forward class decleration , to declear the pointer type in invokeCommand class
//Because getCommand is declear before the declearation of executeCommand class  
class executeCommand ;
//invokeCommand Class decleration
class invokeCommand{
      public :
             invokeCommand();
             executeCommand* getCommand(char *str);
             ~invokeCommand();
             void display();
};
//executeCommand Class decleration
class executeCommand{
      public :
             executeCommand();
             virtual ~executeCommand();
             virtual void display() = 0;
};
//fileCommand Class decleration
class fileCommand : public executeCommand{
      public :
             fileCommand();
             ~fileCommand();
             void display();
};
//fileCommand Class definations
fileCommand::fileCommand(){
                               cout<<"I am in fileCommand ctor"<<endl;
}
fileCommand::~fileCommand(){
                                cout<<"I am in fileCommand dtor"<<endl;
}
void fileCommand::display(){
                                cout<<"I am in fileCommand display "<<endl;
}

//invokeCommand Class definations
invokeCommand::invokeCommand(){
                               cout<<"I am in invoker ctor"<<endl;
}
executeCommand* invokeCommand::getCommand(char *str){
      executeCommand *obj ;
      if(str == "File"){
                             cout<<"I am in paramaterised ctor: File "<<endl;
                             obj = new fileCommand();
      }
      else if(str == "Open")//yet to implement 
                             cout<<"I am in paramaterised ctor: Open "<<endl;
      return obj ;
}
invokeCommand::~invokeCommand(){
                                cout<<"I am in invoker dtor"<<endl;
}
void invokeCommand::display(){
                                cout<<"I am in invokeCommand display "<<endl;
}
//executeCommand Class definations
executeCommand::executeCommand(){
                               cout<<"I am in executeCommand ctor"<<endl;
}
executeCommand::~executeCommand(){
                                cout<<"I am in executeCommand dtor"<<endl;
}
//main function
int main(){
    invokeCommand* objInvoke = new invokeCommand();
    executeCommand* performCommand = objInvoke->getCommand("File") ;
    performCommand->display();
    delete performCommand ;
    delete objInvoke ;
    //int i ;
    //cin>>i;
    }
